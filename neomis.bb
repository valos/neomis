DESCRIPTION = "A computer version of the well-known electronic game named Simon"
HOMEPAGE = "http://code.google.com/p/neomis/"
LICENSE = "GPLv3"
AUTHOR = "Valéry Febvre <vfebvre@easter-eggs.com>"
SECTION = "x11/applications"
PRIORITY = "optional"

SRCREV = "8"
PV = "1.0.3+svnr${SRCPV}"

PACKAGE_ARCH = "all"

SRC_URI = "svn://neomis.googlecode.com/svn;module=trunk;proto=http"
S = "${WORKDIR}/trunk"

inherit distutils

FILES_${PN} += "${datadir}/neomis ${datadir}/applications/neomis.desktop ${datadir}/pixmaps/neomis.png"

RDEPENDS += "python-audio python-pyalsaaudio python-elementary"

do_compile_prepend() {
	${STAGING_BINDIR_NATIVE}/edje_cc -id ${S}/data ${S}/data/neomis.edc
}
