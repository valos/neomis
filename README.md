     _   _                      _
    | \ | | ___  ___  _ __ ___ (_)___
    |  \| |/ _ \/ _ \| '_ ` _ \| / __|
    | |\  |  __/ (_) | | | | | | \__ \
    |_| \_|\___|\___/|_| |_| |_|_|___/


Description
===========
Neomis is a computer version of the well-known electronic game named
Simon.

It's a game of memory and concentration.

You must follow the pattern of sounds and lights as long as you can
remember!

The game will flash quadrants in turn and expect you to repeat the
sequence. If you get the sequence correct, the game will respond with a
longer sequence.

https://gitlab.com/valos/neomis

It's written in Python/Elementary and licensed under
the GNU General Public License v3.


Features
========
- High scores


Authors
=======
Valéry Febvre <vfebvre@easter-eggs.com>


Requirements
============
- python-audio
- python-alsaaudio
- python-elementary


Notes
=====
- Neomis is tested on SHR unstable only.
  Normally, it should run on any system with a revision of
  python-elementary equal or greater to 40756.
- Neomis base home directory is $HOME/.config/neomis.
  This directory and related files are created after first run.


Todo
====
- Difficulty levels ?


Report Bugs
===========
If there is something wrong, please use to the Google code issues:
https://gitlab.com/valos/neomis/issues
Report a bug or look there for possible workarounds.


Credits
=======
- Suzanna Smith and Calum Benson for High Contrast icons.
- Albert Astals Cid, author of the KDE Blinken project, for inspiration
  and WAV sound files.


Version History
===============

2010-11-25 - Version 1.0.3
--------------------------
- Major bugfixes release due to Elementary API changes in List, Toolbar
  and Icon widgets (python-elementary revision >= 53901).

2010-02-14 - Version 1.0.2
--------------------------
- Bug fix release due to a python-elementary API change

2009-11-30 - Version 1.0.1
--------------------------
- Fixed callbacks with new python-elementary (revision >= 43900)
- Nicer 'Stop' and 'Quit' icons

2009-11-10 - Version 1.0.0
--------------------------
- First release
