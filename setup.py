# -*- coding: utf-8 -*-

# Neomis -- A computer version of the well-known electronic game named Simon
#
# Copyright (C) 2009-2010 Valéry Febvre <vfebvre@easter-eggs.com>
# http://code.google.com/p/neomis/
#
# This file is part of Neomis.
#
# Neomis is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Neomis is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
from distutils.core import setup

def main():
    setup(name         = 'neomis',
          version      = '1.0.3',
          description  = 'A computer version of the well-known electronic game named Simon',
          author       = 'Valery Febvre',
          author_email = 'vfebvre@easter-eggs.com',
          url          = 'http://code.google.com/p/neomis/',
          classifiers  = [
            'Development Status :: 2 - Beta',
            'Environment :: X11 Applications',
            'Intended Audience :: End Users/Phone UI',
            'License :: GNU General Public License (GPL)',
            'Operating System :: POSIX',
            'Programming Language :: Python',
            'Topic :: Desktop Environment',
            ],
          packages     = ['neomis'],
          scripts      = ['neomis/neomis'],
          data_files   = [
            ('share/applications', ['data/neomis.desktop']),
            ('share/pixmaps', ['data/neomis.png']),
            ('share/neomis/sounds', [
                    'data/sounds/blue.wav',
                    'data/sounds/green.wav',
                    'data/sounds/red.wav',
                    'data/sounds/yellow.wav',
                    'data/sounds/lose.wav'
                    ]),
            ('share/neomis', ['README', 'data/neomis.edj']),
            ],
          )

if __name__ == '__main__':
    main()
